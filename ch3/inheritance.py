#!/usr/bin/env python
# Source files made while working through
#    Python 3: Object Oriented Programming
#        by Dusty Phillips [packtpub]
# Ch3 - Section 1, basic inheritance


class ContactList(list):
    def search(self, name):
        """Return all contacts that contain the search
        value in their name."""
        matching_contacts = []
        for contact in self:
            if name in contact.name:
                matching_contacts.append(contact)
        return matching_contacts


class Contact:
    all_contacts = ContactList()

    def __init__(self, name='', email='', **kwargs):
        super().__init__(**kwargs)
        self.name = name
        self.email = email
        self.all_contacts.append(self)


class Supplier(Contact):
    def order(self, order):
        print("If this were a real system we would send "
              "{} order to {}.".format(order, self.name))


class LongNameDict(dict):
    def longest_key(self):
        longest = None

        for key in self:
            if not longest or len(key) > len(longest):
                longest = key
        return longest


# Ch3 section 2 - Overriding and Super
# class Friend(Contact):
#     def __init__(self, name, email, phone):
#         super().__init__(name, email)
#         self.phone = phone


# Ch3 section 3 - Multiple Inheritance
class MailSender:
    def send_mail(self, message):
        print("Sending email to " + self.email)
        # add email logic here


class EmailableContact(Contact, MailSender):
    pass


class AddressHolder:
    def __init__(self, street='', city='', state='', code='',
                 **kwargs):
        super().__init__(**kwargs)
        self.street = street
        self.city = city
        self.state = state
        self.code = code


# Ch3 section 4 - Diamond problems
class Friend(Contact, AddressHolder):
    def __init__(self, phone='', **kwargs):
        super().__init__(**kwargs)
        self.phone = phone


class BaseClass:
    num_base_calls = 0

    def call_me(self):
        print("Calling method on base class")
        self.num_base_calls += 1


class LeftSubclass(BaseClass):
    num_left_calls = 0

    def call_me(self):
        super().call_me()
        print("Calling method on left subclass")
        self.num_left_calls += 1


class RightSubclass(BaseClass):
    num_right_calls = 0

    def call_me(self):
        super().call_me()
        print("Calling method on right subclass")
        self.num_right_calls += 1


class Subclass(LeftSubclass, RightSubclass):
    num_sub_calls = 0

    def call_me(self):
        super().call_me()
        print("Calling method on subclass")
        self.num_sub_calls += 1


# main method check
if __name__ == "__main__":
    print("For Ch3 - Basic inheritance, overriding(super), ."
          "and multiple inheritance.")
